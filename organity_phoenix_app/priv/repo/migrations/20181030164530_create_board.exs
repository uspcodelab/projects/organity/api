defmodule Api.Repo.Migrations.CreateBoard do
  use Ecto.Migration

  def change do
    create table(:boards) do 
      add :name, :string
      add :user_id, references(:users)
    end

    create unique_index(:boards, [:name, :user_id], name: :board_idx)
  end

end
