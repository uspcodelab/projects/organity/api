defmodule Api.Repo.Migrations.CreateLists do
  use Ecto.Migration

  def change do
    create table(:lists) do 
      add :name, :string
      add :board_id, references(:boards)
    end

    create unique_index(:lists, [:name, :board_id], name: :list_idx)
  end
end
