defmodule Api.List do
  use Ecto.Schema
  import Ecto.Changeset

  schema "lists" do
    field :name, :string
    belongs_to :board, Api.Board
  end

  def changeset(board, params \\ %{}) do
    board 
    |> cast(params, [:name])
    |> validate_required([:name])
    |> unique_constraint(:list_idx)
  end

end