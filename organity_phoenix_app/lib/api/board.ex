defmodule Api.Board do
  use Ecto.Schema
  import Ecto.Changeset

  schema "boards" do
    field :name, :string
    belongs_to :user, Api.User
    has_many :lists, Api.List, on_delete: :delete_all
  end

  def changeset(board, params \\ %{}) do
    board 
    |> cast(params, [:name])
    |> validate_required([:name])
    |> unique_constraint(:board_idx)
    |> cast_assoc(:lists)
  end

end