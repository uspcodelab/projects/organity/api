defmodule Api.User do
  use Ecto.Schema 
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :name, :string 
    field :password, :string
    has_many :boards, Api.Board, on_delete: :delete_all
  end

  @email_re ~r/^[A-Za-z0-9._%+-+']+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/
  @name_re ~r/^[A-Z\sa-z]+$/
  def changeset(user, params \\%{}) do
      user 
      |> cast(params, [:email, :name, :password])
      |> validate_required([:email, :name, :password])
      |> validate_format(:name, @name_re)
      |> validate_format(:email, @email_re)
      |> unique_constraint(:email)
      |> validate_length(:password, min: 6)
      |> cast_assoc(:boards)
  end
      
end