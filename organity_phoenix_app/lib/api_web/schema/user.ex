defmodule Api.Schema.User do
  use Absinthe.Schema.Notation

  alias Api.{User, Resolvers}

  object :user_queries do
    field :user, :user do
      arg :id, :id
      resolve &Resolvers.User.get_user/3
     end
  end

  object :user_mutations do
    field :create_user, :user do 
      arg :input, :user_input
      resolve &Resolvers.User.create_user/3
    end

    field :edit_user, :user do 
      arg :id, :id
      arg :input, :user_input
      resolve &Resolvers.User.update_user/3
    end

    field :delete_user, :user do 
      arg :id, :id
      resolve &Resolvers.User.delete_user/3
    end
  end

  object :user do 
    field :id, :id
    field :email, :string
    field :name, :string
    field :password, :string
  end

  input_object :user_input do
    field :email, :string
    field :name, :string
    field :password, :string
  end
end