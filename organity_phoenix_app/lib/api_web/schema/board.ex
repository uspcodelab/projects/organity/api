defmodule Api.Schema.Board do
  use Absinthe.Schema.Notation

  alias Api.{User, Resolvers}
  
  object :board_queries do
    field :all_boards, list_of(:board) do
      arg :user_id, :id
      resolve &Resolvers.Board.all_boards/3
    end

    field :board, :board do
      arg :id, :id 
      resolve &Resolvers.Board.get_board/3
    end
  end

  object :board_mutations do
    field :create_board, :board do 
      arg :user_id, :id
      arg :input, :board_input 
      resolve &Resolvers.Board.create_board/3
    end

    field :edit_board, :board do 
      arg :id, :id
      arg :input, :board_input 
      resolve &Resolvers.Board.update_board/3
    end

    field :delete_board, :board do 
      arg :id, :id
      resolve &Resolvers.Board.delete_board/3
    end
  end
  
  object :board do 
    field :id, :id
    field :name, :string
  end

  input_object :board_input do
    field :name, :string
  end
end