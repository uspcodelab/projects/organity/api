defmodule Api.Schema do
  use Absinthe.Schema

  import_types __MODULE__.User
  import_types __MODULE__.Board
  import_types __MODULE__.List

  query do
    import_fields :user_queries
    import_fields :board_queries
    import_fields :list_queries
  end

  mutation do
    import_fields :user_mutations
    import_fields :board_mutations
    import_fields :list_mutations
  end
end