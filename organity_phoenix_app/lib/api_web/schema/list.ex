defmodule Api.Schema.List do
  use Absinthe.Schema.Notation

  alias Api.{Board, Resolvers}
  
  object :list_queries do
    field :all_lists, list_of(:list) do
      arg :board_id, :id
      resolve &Resolvers.List.all_lists/3
    end

    field :list, :list do
      arg :id, :id 
      resolve &Resolvers.List.get_list/3
    end
  end

  object :list_mutations do
    field :create_list, :list do 
      arg :board_id, :id
      arg :input, :list_input 
      resolve &Resolvers.List.create_list/3
    end

    field :edit_list, :list do 
      arg :id, :id
      arg :input, :board_input 
      resolve &Resolvers.List.update_list/3
    end

    field :delete_list, :list do 
      arg :id, :id
      resolve &Resolvers.List.delete_list/3
    end
  end
  
  object :list do 
    field :id, :id
    field :name, :string
  end

  input_object :list_input do
    field :name, :string
  end
end