defmodule Api.Resolvers.User do
  alias Api.{User, Repo}

  def get_user(_, %{id: id}, _) do
    {:ok, Repo.get(User, id)}
  end

  def create_user(_, %{input: params}, _) do
    create = fn attrs -> 
        %User{} 
        |> User.changeset(attrs)
        |> Repo.insert 
    end

    case create.(params) do 
        {:error, changeset} ->
          {:error, 
            message: "Could not create user", 
            details: error_details(changeset)
          }
        {:ok, _} = success -> 
          success
     end
  end


  def update_user(_, %{id: id, input: params}, _) do
    update = fn (uid, attrs) ->
      User 
      |> Repo.get(uid)
      |> User.changeset(attrs)
      |> Repo.update
    end

    case update.(id, params) do
      {:error, changeset} ->
        {:error, 
          message: "Could not update user", 
          details: error_details(changeset)
        }
      {:ok, _} = success ->
        success
    end
  end

  def delete_user(_, %{id: id}, _) do
    delete = fn id -> 
      User 
      |> Repo.get(id) 
      |> Repo.delete()
    end

    case delete.(id) do
      {:error, changeset} ->
        {:error, 
          message: "Could not delete user",
          details: error_details(changeset)
        }
      {:ok, _} = success ->
        success
    end
  end

  defp error_details(changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(fn {msg, _} -> msg end)
  end
end
