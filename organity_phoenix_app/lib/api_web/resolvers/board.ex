defmodule Api.Resolvers.Board do
  alias Api.{User, Repo, Board}
  import Ecto.Query

  def get_board(_, %{id: id}, _) do
    {:ok, Repo.get(Board, id)}
  end

  def all_boards(_, %{user_id: uid}, _) do
    boards = Board 
            |> Ecto.Query.where(user_id: ^uid)
            |> Repo.all
    {:ok, boards}
  end

  def create_board(_, %{user_id: uid, input: params}, _) do
    create = fn uid, attrs -> 
      user = Repo.get(User, uid)
      Ecto.build_assoc(user, :boards, attrs)
      |> Board.changeset()
      |> Repo.insert 
    end

    case create.(uid, params) do 
        {:error, changeset} ->
          {:error, 
            message: "Could not create board", 
            details: error_details(changeset)
          }
        {:ok, _} = success -> 
          success
     end
  end


  def update_board(_, %{id: id, input: params}, _) do
    update = fn (id, attrs) ->
      Board
      |> Repo.get(id)
      |> Board.changeset(attrs)
      |> Repo.update
    end

    case update.(id, params) do
      {:error, changeset} ->
        {:error, 
          message: "Could not update board", 
          details: error_details(changeset)
        }
      {:ok, _} = success ->
        success
    end
  end

  def delete_board(_, %{id: id}, _) do
    delete = fn id -> 
      Board 
      |> Repo.get(id) 
      |> Repo.delete()
    end

    case delete.(id) do
      {:error, changeset} ->
        {:error, 
          message: "Could not delete board",
          details: error_details(changeset)
        }
      {:ok, _} = success ->
        success
    end
  end

  defp error_details(changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(fn {msg, _} -> msg end)
  end
end
