defmodule Api.Resolvers.List do
  alias Api.{Repo, Board, List}
  import Ecto.Query

  def get_list(_, %{id: id}, _) do
    {:ok, Repo.get(List, id)}
  end

  def all_lists(_, %{board_id: bid}, _) do
    lists = List 
            |> Ecto.Query.where(board_id: ^bid)
            |> Repo.all
    {:ok, lists}
  end

  def create_list(_, %{board_id: bid, input: params}, _) do
    create = fn bid, attrs -> 
      board = Repo.get(Board, bid)
      Ecto.build_assoc(board, :lists, attrs)
      |> List.changeset()
      |> Repo.insert 
    end

    case create.(bid, params) do 
        {:error, changeset} ->
          {:error, 
            message: "Could not create list", 
            details: error_details(changeset)
          }
        {:ok, _} = success -> 
          success
     end
  end


  def update_list(_, %{id: id, input: params}, _) do
    update = fn (id, attrs) ->
      List 
      |> Repo.get(id)
      |>  List.changeset(attrs)
      |> Repo.update
    end

    case update.(id, params) do
      {:error, changeset} ->
        {:error, 
          message: "Could not update list", 
          details: error_details(changeset)
        }
      {:ok, _} = success ->
        success
    end
  end

  def delete_list(_, %{id: id}, _) do
    delete = fn id -> 
      List
      |> Repo.get(id) 
      |> Repo.delete()
    end

    case delete.(id) do
      {:error, changeset} ->
        {:error, 
          message: "Could not delete list",
          details: error_details(changeset)
        }
      {:ok, _} = success ->
        success
    end
  end

  defp error_details(changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(fn {msg, _} -> msg end)
  end
end
