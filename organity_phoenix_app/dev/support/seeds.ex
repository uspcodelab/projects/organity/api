defmodule Api.Seeds do
  alias Api.{Repo, User}

  @lists [
      %{name: "List 1"}, 
      %{name: "List 2"},
      %{name: "List 3"} 
  ]

  @boards [ 
      %{name: "Board 1",
        lists: @lists
        }, 
      %{name: "Board 2"},
      %{name: "Board 3"} 
    ]

  @user %{email: "seed@seed.com", name: "Seed", password: "123456", 
    boards: @boards}

  def run() do
    User.changeset(%User{}, @user)
    |> Repo.insert
  end
end