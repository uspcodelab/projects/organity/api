defmodule Api.Schema.Query.BoardTest do
  use ApiWeb.ConnCase, async: true
  require Logger

  alias Api.{Repo, User}

  setup  do
    Api.Seeds.run()
    usr = Repo.get_by(User, email: "seed@seed.com")
    {:ok, %{user: usr}}
  end

  test "all boards returns all boards", context do
    id = context[:user] |> Map.get(:id)
    query = """
    {
      allBoards(userId: #{id}) {
        name
      }
    }
    """
    conn = build_conn()
    conn = get conn, "/api", query: query
    assert json_response(conn, 200) == %{
      "data" => %{
        "allBoards" => [
          %{"name" => "Board 1"}, 
          %{"name" => "Board 2"}, 
          %{"name" => "Board 3"}, 
        ]
      }
    }
  end
end