defmodule Api.Schema.Query.BoardTest do
  use ApiWeb.ConnCase, async: true
  require Logger

  alias Api.{Repo, Board}

  setup  do
    Api.Seeds.run()
    board = Repo.get_by(Board, name: "Board 1")
    {:ok, %{board: board}}
  end

  test "all lists returns all lists", context do
    id = context[:board] |> Map.get(:id)
    query = """
    {
      allLists(boardId: #{id}) {
        name
      }
    }
    """
    conn = build_conn()
    conn = get conn, "/api", query: query
    assert json_response(conn, 200) == %{
      "data" => %{
        "allLists" => [
          %{"name" => "List 1"}, 
          %{"name" => "List 2"}, 
          %{"name" => "List 3"}, 
        ]
      }
    }
  end
end