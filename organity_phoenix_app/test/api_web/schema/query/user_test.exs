defmodule Api.Schema.Query.UserTest do
  use ApiWeb.ConnCase, async: true
  require Logger

  alias Api.{Repo, User}

  setup do
    Api.Seeds.run()
    :ok
  end


 

  
  test "user field return a user" do
    id = User |> Repo.get_by(email: "seed@seed.com") |> Map.get(:id)
    query = """
    {
      user(id: #{id}) {
        email
        name
      }
    }
    """
    conn = build_conn()
    conn = get conn, "/api", query: query
    assert json_response(conn, 200) == %{
      "data" => %{
        "user" => %{
          "email" => "seed@seed.com", 
          "name" => "Seed"
        }
      }
    }
  end
end