defmodule ApiWeb.Schema.Mutation.CreateUserTest do
  use ApiWeb.ConnCase, async: true
  alias Api.{Repo, User}

  setup do
    
    :ok
  end

  @query """
  mutation CreateUser($user: UserInput){
    createUser(input: $user) {
   	 	email
    	name
  	}
  }
  """
 
  test "createUser field creates an user" do
    user = %{
      "email" => "novo_email@gmail.com",
      "name" => "Novo Nome", 
      "password" => "123456"
    }

    conn = build_conn() 
    conn = post conn, "/api", query: @query, variables: %{"user" => user}

    assert json_response(conn, 200) == %{
       "data" => %{
        "createUser" => %{
          "email" => "novo_email@gmail.com", 
          "name" => "Novo Nome"
        }
      }
    }
  end

  test "creating a user with an existing email fails" do
    Api.Seeds.run()

    user = %{
      "email" => "seed@seed.com",
      "name" => "Novo nome", 
      "password" => "123456"
    }

    conn = build_conn()
    conn = post conn, "/api", 
      query: @query, 
      variables: %{"user" => user}

    assert json_response(conn, 200) == %{
      "data" => %{"createUser" => nil}, 
      "errors" => [
        %{
          "locations" => [%{"column" => 0, "line" => 3}], 
          "message" => "Could not create user", 
          "details" => %{"email" => ["has already been taken"]},
          "path" => ["createUser"]
        }
      ]
    }
  end

  
end
