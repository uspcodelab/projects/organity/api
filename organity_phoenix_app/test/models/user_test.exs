defmodule Api.Models.UserTest do
  use Api.DataCase
  alias Api.User

  @valid_attrs %{email: "email@email.com", name: "Pat Example", password: "123456"}

  test "valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid? 
  end

  @invalid_emails [%{email: "@email.com", name: "Pat Example", password: "123456"}, 
                   %{email: "emailemaicom", name: "Pat Example", password: "123456"},
                   %{email: "", name: "Pat Example", password: "123456"}
                  ]
  test "invalid email" do
    Enum.each @invalid_emails, fn user -> 
      changeset = User.changeset(%User{}, user)
      refute changeset.valid?, "Error in user: " <> inspect(user)
    end
  end

  test "unique email" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert {:ok, ch} = Api.Repo.insert(changeset) 

    changeset = User.changeset(%User{}, %{@valid_attrs | name: "Changed"})
    assert {:error, ch} = Api.Repo.insert(changeset)
  end

  @invalid_pass %{email: "email@email.com", name: "Pat Example", password: "12345"}
  test "password length" do
    changeset = User.changeset(%User{}, @invalid_pass)
    refute changeset.valid? 
  end

  @required_attr [%{name: "Pat Example", password: "123456"}, 
                   %{email: "email@email.com", password: "123456"},
                   %{email: "email@email.com", name: "Pat Example"}
                  ]
  test "required attributes" do
    Enum.each @required_attr, fn user -> 
      changeset = User.changeset(%User{}, user)
      refute changeset.valid?, "Error in user: " <> inspect(user)
    end
  end

  @invalid_names [%{email: "email@email.com", name: "123", password: "123456"}, 
                   %{email: "email@email.com", name: "", password: "123456"}, 
                   %{email: "email@email.com", name: "Joao_Houls", password: "123456"}
                 ]
  test "invalid name" do
    Enum.each @invalid_names, fn user -> 
      changeset = User.changeset(%User{}, user)
      refute changeset.valid?, "Error in user: " <> inspect(user)
    end
  end

end