defmodule Api.Models.BoardTest do
  use Api.DataCase
  alias Api.{Repo, Board, List}

  setup  do
    Api.Seeds.run()
    board = Repo.get_by(Board, name: "Board 1")
    {:ok, %{board: board}}
  end

  test "valid attributes", context do
    board = context[:board] 
    changset = Ecto.build_assoc(board, :lists, %{name: "List name"})
    |> List.changeset

    assert changset.valid?
  end

  test "invalid attributes", context do
    board = context[:board] 
    changset = Ecto.build_assoc(board, :lists, %{name: ""})
    |> List.changeset


    refute changset.valid?
  end

end