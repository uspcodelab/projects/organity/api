
defmodule Api.Models.BoardTest do
  use Api.DataCase
  alias Api.{Repo, User, Board}

  setup  do
    Api.Seeds.run()
    usr = Repo.get_by(User, email: "seed@seed.com")
    {:ok, %{user: usr}}
  end

  test "valid attributes", context do
    usr = context[:user] 
    changset = Ecto.build_assoc(usr, :boards, %{name: "Name"})
    |> Board.changeset

    assert changset.valid?
  end

  test "invalid attributes", context do
    usr = context[:user] 
    changset = Ecto.build_assoc(usr, :boards, %{name: ""})
    |> Board.changeset

    refute changset.valid?
  end

  test "new user with valid board" do
    usr = %{email: "email@email.com", name: "Pat Example", password: "123456",
          boards: [%{name: "Name"}]}
    changset = %User{}
              |> User.changeset(usr)
    assert changset.valid?
  end

  test "new user with invalid board" do
    usr = %{email: "email@email.com", name: "Pat Example", password: "123456",
          boards: [%{name: ""}]}
    changset = %User{}
              |> User.changeset(usr)
    refute changset.valid?
  end


end